<?php
	// footer can be setup to a maximum of 3 columns

	$sidebars = (int) is_active_sidebar('hrb-footer') + (int) is_active_sidebar('hrb-footer2') + (int) is_active_sidebar('hrb-footer3') + (int) is_active_sidebar('hrb-footer4');

	if ( ! $sidebars ) { $sidebars = 1;	}

	$columns = 12 / $sidebars;
?>

<div class="footer">
	<div class="wrapper">
    	<div class="ftr_box">
        	<?php dynamic_sidebar('hrb-footer'); ?>
        </div>
        <div class="ftr_box">
        	<?php dynamic_sidebar('hrb-footer2'); ?>
        </div>        
        <div class="ftr_box">
        	<?php dynamic_sidebar('hrb-footer3'); ?>
        </div>
        <div class="ftr_box">
        	<?php dynamic_sidebar('hrb-footer4'); ?>
        </div>
        
        <div class="footer_bottom">
        	<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/footer_logo.png" alt=""></a>
            <div class="copyright">
            	&copy; 2011-<?php echo date("Y"); ?>. All rights reserved
            </div>
        </div>
    </div>
</div>
<!-- Footer Widgets -->
