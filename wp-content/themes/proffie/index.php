<?php
/**
 * Template Name: Projects & Freelancers Listings
 */
?>
<section class="banner">
	<div class="wrapper">
    	<div class="fl">
        	<?php the_post_thumbnail( "full" ); ?> 
        </div>
        <div class="fr">
        	<?php the_content(); ?>
        </div>
        <div class="cl"></div>
    </div>
</section>
<?php 
$args = array(
	'type'                     => 'project',
	'child_of'                 => 0,
	'parent'                   => 0,
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'hide_empty'               => 0,
	'hierarchical'             => 1,
	'taxonomy'                 => 'project_category',
); 
$categories = get_categories( $args ); 
if($categories):?> 
    <div class="container categories_box">
        <div class="wrapper">
            <h2>Top Categories</h2>	
            <div class="icon_box">
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/menu_list_bg.png" alt="">
            </div>
            <div class="categ_box">
                <ul>
                	<?php foreach($categories as $category): ?>
                    	<li><a href="<?php echo get_category_link($category->term_id); ?>"><i><?php if (function_exists('z_taxonomy_image_url')) ?> <img src="<?php echo z_taxonomy_image_url($category->term_id); ?>"  /></i><?php echo $category->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php $WhyProffie = get_post(48);
if($WhyProffie):
 ?>
<div class="container proffie_box">
	<div class="rightside_img">
    	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ubm_img.png" alt="">
    </div>
	<div class="wrapper">
    	<h2><?php print_r($WhyProffie->post_title); ?></h2>
        <h4><?php print_r($WhyProffie->post_content); ?></h4>
        <div class="icon_box">
        	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon_u.png" alt="">
        </div>
        <?php
		  $args = array(
			  'orderby' => 'menu_order',
				'order' => 'ASC',
				'post_type' => 'page',
				'post_status' => 'publish',
				'suppress_filters' => true,
				'posts_per_page' => -1,
			  	'post_parent'  => 48,
		  ); 
		  $ProffieTexts = new WP_Query( $args ); 
		  if($ProffieTexts):?>
			  <div class="prof_box">
				  <ul class="clearfix">
                  	<?php while($ProffieTexts->have_posts()) : $ProffieTexts->the_post(); ?>
					  <li>
						  <div class="prof_cont">
							  <div class="fl">
								  <?php the_post_thumbnail( "full" ); ?> 
							  </div>
							  <div class="fr">
								  <h3><?php the_title(); ?></h3>
								  <?php the_content(); ?>
							  </div>
							  <div class="cl"></div>
						  </div>
					  </li>
					<?php endwhile; ?>  
				  </ul>
			  </div>
		  <?php endif; ?>
    </div>
</div>
<?php endif; ?>
<?php $args = array(
	'orderby' => 'menu_order',
	  'order' => 'ASC',
	  'post_type' => 'page',
	  'post_status' => 'publish',
	  'suppress_filters' => true,
	  'posts_per_page' => 1,
	  'page_id'  => 7,
); 
$HowWorks = new WP_Query( $args ); 
if($HowWorks):?>
	<div class="container how_it_works_box">
    	<?php while($HowWorks->have_posts()) : $HowWorks->the_post(); ?>
			<div class="wrapper">
    			<h2><?php the_title(); ?></h2>	
                <div class="icon_box">
                    <?php the_post_thumbnail( "full" ); ?> 
                </div>
                <div class="works_box">
                    <?php the_content(); ?>
                </div>
			</div>
       <?php endwhile; ?>
	</div>        
<?php endif; ?>
<?php $args = array(
	'orderby' => 'menu_order',
	  'order' => 'ASC',
	  'post_type' => 'page',
	  'post_status' => 'publish',
	  'suppress_filters' => true,
	  'posts_per_page' => 1,
	  'page_id'  => 73,
); 
$WhatProffie = new WP_Query( $args ); 
if($WhatProffie):?>
	<div class="container">
    	<?php while($WhatProffie->have_posts()) : $WhatProffie->the_post(); ?>
			<div class="wrapper">
    			<h2><?php the_title(); ?></h2>	
                <div class="icon_box">
                    <?php the_post_thumbnail( "full" ); ?> 
                </div>
                <div class="proffee_box">
                    <?php the_content(); ?>
                    <div class="cl"></div>
                </div>
			</div>
       <?php endwhile; ?>
	</div>      </div>  
<?php endif; ?>
<?php $args = array(
	'orderby' => 'menu_order',
	  'order' => 'ASC',
	  'post_type' => 'page',
	  'post_status' => 'publish',
	  'suppress_filters' => true,
	  'posts_per_page' => 3,
	  'post__in'  => array(76,83,85),
); 
$HowProffie = new WP_Query( $args ); 
if($HowProffie):?>
<div class="container">
	<div class="wrapper">
    	<div class="box_div">
        	<ul>
            	<?php while($HowProffie->have_posts()) : $HowProffie->the_post(); ?>
                	<li>
                        <div class="box_div_cnt">
                            <i><?php the_post_thumbnail( "full" ); ?> </i>
                            <h3><?php the_title(); ?></h3>
                            <?php the_content(); ?>
                            <div class="btn_box">
                                <a href="<?php the_permalink(); ?>" class="btn_link">learn more</a>
                            </div>
                        </div>    
                    </li>
                <?php endwhile; ?>
            </ul>
	</div>
</div>
</div>                
<?php endif; ?>
<?php $args = array(
	'orderby' => 'menu_order',
	  'order' => 'ASC',
	  'post_type' => 'page',
	  'post_status' => 'publish',
	  'suppress_filters' => true,
	  'posts_per_page' => 2,
	  'post__in'  => array(87,91),
); 
$HowProffie = new WP_Query( $args ); 
if($HowProffie):?>
<div class="cross_box_coamn">
	<?php 
		$x = 1;
		while($HowProffie->have_posts()) : $HowProffie->the_post();
		$url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>
    	<div class="cross_div_box<?php if($x%2==0): ?>2<?php endif; ?>">
	<div class="fl" style="background:url('<?php echo $url; ?>') no-repeat right; background-size:100% auto;">
    	<!--<img src="images/img-box.jpg" alt="">-->
    </div>
    <div class="fr">
    	<div class="rgt_table">
        	<div class="rgt_cnt">
            	<h3><?php the_title(); ?></h3>
               	<?php the_content(); ?>
                <div class="btn_box">
                	<a href="<?php the_permalink(); ?>" class="btn_link">watch tv clip</a>
                </div>
            </div>
        </div>
    </div>
    <div class="cl"></div>
</div>
    <?php 
	$x = $x +1;
	endwhile; ?>
</div>
<?php endif; ?>