<!-- Header ad space -->
<?php hrb_display_ad_sidebar( 'hrb-header', $position = 'header' ); ?>

<section class="header homeheader">
	<div class="wrapper">
    	<div class="logo">
        	<?php the_hrb_logo(); ?>
        </div>
        <div class="header_right">
        	<div class="nav">
            	<div class="bodyclose"></div>
                <div class="toggle"></div>
                <?php the_hrb_nav_menu(); ?>
            </div>
            <div class="enter_box">
            	<?php do_action( 'hrb_before_user_nav_links' ); ?>
				<?php the_hrb_user_nav_links(); ?>
				<?php do_action( 'hrb_after_user_nav_links' ); ?>
            </div>
        </div>
        <div class="cl"></div>
    </div>
</section>