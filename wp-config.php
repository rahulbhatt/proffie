<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'profie');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*^cbQ MR+t;0iv<%A*MHPX.mO@]:nIgpQrw-r<%h-ic-;>Wowidw~eY}Sr5Mz83+');
define('SECURE_AUTH_KEY',  'c`xw|a^!7hdJ*QL`tw`M/Wk .6/ tuF@NTWw9fb.}rN=i=Fcquc-T:sY#I,+IuN<');
define('LOGGED_IN_KEY',    'HYH(#!Nsb$@_.H-xEUpJ8N$5c-+-|?w{L=!%Y+D0$Q]&x%Lc{1hddbQYp.jX4F^5');
define('NONCE_KEY',        '3D3b9$O)M#oP;Fajs+]I^x 8kA+W`Q)_+XZ0722BVANg $J4-U>+(ja{iI;3eBG%');
define('AUTH_SALT',        '!rSd5FNdj=z>aFy+6o=m+PC0|Nl{BOlqcWK#J:<vFdZ~7->X>vm-P+k|gfusmH!)');
define('SECURE_AUTH_SALT', '!<56c--V6UN VG+8]r1FkkC3[IkW1|hl?s<Y};mB*yC_.V&3ywc ^|_4HSh,8n.V');
define('LOGGED_IN_SALT',   'Wl &zOPN<[],+{8/fMMY-YmZo>HYxfs%#-Cx<DztCfX7!gA|3J Mm:VdS3E:F?u^');
define('NONCE_SALT',       'bm=9b!LV9:bI+*d]A<D$/&KyZ7Al?nWvaK(4e+Y:N6+n`8ZR,}9:#(n(k66$Y7nG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'proffie_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
